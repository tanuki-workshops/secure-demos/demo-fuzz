let tools = require('./my-tools')

function fuzz(buf) {
  const text = buf.toString()
  tools.readmeContent(text)
}

module.exports = {
    fuzz
}

