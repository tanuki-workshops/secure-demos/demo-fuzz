const fs = require('fs')

function sayHello(name) {
  if(name.includes("z")) {
    //throw new Error("😡 error name: " + name)
    console.log("😡 error name: " + name)
  } else {
    return "😀 hello " + name
  }
}

function readmeContent(name) {

  let fileName = name => {
    if(name.includes("w")) {
      return "./README.txt"
    } else {
      return "./README.md"
    }
  }

  //const data = fs.readFileSync(fileName(name), 'utf8')
  try {
    const data = fs.readFileSync(fileName(name), 'utf8')
    return data
  } catch (err) {
    console.error(err.message)
    return ""
  }

}

module.exports = {
  sayHello, readmeContent
}


